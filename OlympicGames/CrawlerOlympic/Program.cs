﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrawlerOlympic.model;
using System.Net;
using HtmlAgilityPack;
using System.Text.RegularExpressions;

namespace CrawlerOlympic
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                var BaseAccess = new BaseAccess();
                BaseAccess.host = "http://data.rio";
                BaseAccess.group = "/dataset/rio-casas-hospitalidade-hospitality-houses";
                BaseAccess.pathFile = @"C:\Users\Robson\Documents\OlympicGames\olympicgame\OlympicGames";

                var pattern = new Regex(@"http://dadosabertos.rio.rj.gov.br/RioCasasHospitalidade/api/v1/csv/");

                 
                var client = new WebClient();

                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                client.Encoding = System.Text.Encoding.UTF8;

                var htmlDoc = new HtmlDocument();

                htmlDoc.LoadHtml(client.DownloadString(BaseAccess.host + BaseAccess.group));

                foreach (var href in htmlDoc.DocumentNode.Descendants("a").Select(x => x.Attributes["href"]))
                {
                    if (pattern.IsMatch(href.Value))
                        client.DownloadFile(href.Value,BaseAccess.pathFile);

                }

                Console.WriteLine("Fim");  
                Console.ReadKey();
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
                Console.ReadKey();
            }

        }
    }
}
