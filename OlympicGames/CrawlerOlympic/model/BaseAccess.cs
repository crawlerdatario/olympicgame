﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrawlerOlympic.model
{
    public class BaseAccess
    {
        public string host { get; set; }
        public string group { get; set; }
        public string pathFile { get; set; }
    }
}
